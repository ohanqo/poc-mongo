import Post from "../models/Post";
import Comment from "../models/Comment";

export default class PostController {
  static async create(req, res) {
    let status = 200;
    let body = [];

    try {
      const { title, content, userId } = req.body;

      let post = await Post.create({
        title,
        content,
        userId
      });

      body = {
        post,
        message: "Post created"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async index(req, res) {
    let status = 200;
    let body = {};

    try {
      let posts = await Post.find().populate("userId");

      body = {
        posts,
        message: "Posts list"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async show(req, res) {
    let status = 200;
    let body = {};

    try {
      let id = req.params.id;
      let post = await Post.findById(id).populate("userId");
      let comments = await Comment.find({ postId: id });

      body = {
        post,
        comments,
        message: "Post information"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async update(req, res) {
    let status = 200;
    let body = {};

    try {
      let id = req.params.id;
      // Méthode 1
      // let Post = await Post.findById(id);

      // Object.assign(Post, req.body);
      // await Post.save();

      // Méthode 2
      let post = await Post.findByIdAndUpdate(
        id,
        { $set: req.body },
        { new: true }
      );

      body = {
        post,
        message: "Post information"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async delete(req, res) {
    let status = 204;
    let body = {};

    try {
      let id = req.params.id;
      await Post.findByIdAndDelete({ _id: id });

      body = {
        message: "Post deleted"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }
}
