import Comment from "../models/Comment";

export default class CommentController {
  static async create(req, res) {
    let status = 200;
    let body = [];

    try {
      const { content, userId, postId, createdAt } = req.body;

      let comment = await Comment.create({
        content,
        userId,
        postId,
        createdAt
      });

      body = {
        comment,
        message: "Comment created"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async index(req, res) {
    let status = 200;
    let body = {};

    try {
      let comments = await Comment.find()
        .populate("userId")
        .populate("postId");

      body = {
        comments,
        message: "Comments list"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async show(req, res) {
    let status = 200;
    let body = {};

    try {
      let id = req.params.id;
      let comment = await Comment.findById(id).populate("userId");

      body = {
        comment,
        message: "Comment information"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async update(req, res) {
    let status = 200;
    let body = {};

    try {
      let id = req.params.id;
      // Méthode 1
      // let Comment = await Comment.findById(id);

      // Object.assign(Comment, req.body);
      // await Comment.save();

      // Méthode 2
      let comment = await Comment.findByIdAndUpdate(
        id,
        { $set: req.body },
        { new: true }
      );

      body = {
        comment,
        message: "Comment information"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async delete(req, res) {
    let status = 204;
    let body = {};

    try {
      let id = req.params.id;
      await Comment.findByIdAndDelete({ _id: id });

      body = {
        message: "Comment deleted"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }
}
