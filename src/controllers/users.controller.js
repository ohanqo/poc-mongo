import User from "../models/User";
import Post from "../models/Post";
import jwt from "jsonwebtoken";

export default class UserController {
  static async create(req, res) {
    let status = 200;
    let body = [];

    try {
      const { firstname, lastname, email } = req.body;

      let user = await User.create({
        firstname,
        lastname,
        email
      });

      body = {
        user,
        message: "User created"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async index(req, res) {
    let status = 200;
    let body = {};

    try {
      let users = await User.find();

      body = {
        users,
        message: "Users list"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async show(req, res) {
    let status = 200;
    let body = {};

    try {
      let id = req.params.id;
      let user = await User.findById(id);
      let posts = await Post.find({ userId: id });

      body = {
        user,
        posts,
        message: "User information"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async update(req, res) {
    let status = 200;
    let body = {};

    try {
      let id = req.params.id;
      // Méthode 1
      // let user = await User.findById(id);

      // Object.assign(user, req.body);
      // await user.save();

      // Méthode 2
      let user = await User.findByIdAndUpdate(
        id,
        { $set: req.body },
        { new: true }
      );

      body = {
        user,
        message: "User information"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async delete(req, res) {
    let status = 204;
    let body = {};

    try {
      let id = req.params.id;
      await User.findByIdAndDelete({ _id: id });

      body = {
        message: "User deleted"
      };
    } catch ({ message }) {
      status = 500;
      body = { message };
    }

    return res.status(status).json(body);
  }

  static async login(req, res) {
    let status = 200;
    let body = {};

    try {
      const { email, password } = req.body;
      const user = await User.findOne({ email });

      if (user && user.password === password) {
        const token = jwt.sign(
          {
            sub: user._id
          },
          "monsecret"
        );

        body = {
          user,
          token,
          message: "User authenticated"
        };
      } else {
        status = 401;
        body = {
          message: "Erreur d'email ou mot de passe."
        };
      }
    } catch (error) {
      status = 500;
      body = {
        message: "User not authenticated"
      };
    }

    return res.status(status).json(body);
  }
}
