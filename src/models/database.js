import mongoose from "mongoose";

const connectDatabse = () =>
  mongoose.connect("mongodb://localhost:27017/blog", {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

export default connectDatabse;
