import { Router } from "express";
import UserController from "../controllers/users.controller";
import PostController from "../controllers/posts.controller";
import CommentController from "../controllers/comments.controller";
import Auth from "../config/auth";

const router = Router();

router.get("/test", (req, res) => {
  res.send("test");
});

router.post("/login", UserController.login);
router.get("/users", Auth.auth([1, 10]), UserController.index);
router.get("/users/:id", UserController.show);
router.post("/users", UserController.create);
router.delete("/users/:id", UserController.delete);
router.put("/users/:id", UserController.update);

router.get("/posts", PostController.index);
router.get("/posts/:id", PostController.show);
router.post("/posts", PostController.create);
router.delete("/posts/:id", PostController.delete);
router.put("/posts/:id", PostController.update);

router.get("/comments", CommentController.index);
router.get("/comments/:id", CommentController.show);
router.post("/comments", CommentController.create);
router.delete("/comments/:id", CommentController.delete);
router.put("/comments/:id", CommentController.update);

export default router;
