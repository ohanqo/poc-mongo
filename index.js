import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import router from "./src/routes/routes";
import connectDatabse from "./src/models/database";
import jwt from "./src/config/jwt";

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors({ origin: true }));
app.use(jwt());

app.use(router);

const port = 3000;

connectDatabse().then(() => {
  console.log("Database is connecting…");

  app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
  });
});
